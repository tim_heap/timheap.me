#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Tim Heap'
AUTHOR_EMAIL = 'heap.tim@gmail.com'
SITENAME = 'Tim Heap'
SITEURL = ''

TIMEZONE = 'Australia/Hobart'
DEFAULT_LANG = 'en'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'

PATH = 'content'
PAGE_DIR = 'pages'
ARTICLE_DIR = 'articles'

DELETE_OUTPUT_DIRECTORY = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS =  ()

# Social widget
SOCIAL = (
    ('@tim_heap', 'https://twitter.com/tim_heap'),
)

THEME = 'theme'
DEFAULT_PAGINATION = 5

RELATIVE_URLS = False

PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)

ARTICLE_URL = ('b/{slug}/')
ARTICLE_SAVE_AS = ('b/{slug}/index.html')

PLUGIN_PATH = 'plugins'
PLUGINS = []
PYGMENTS_RST_OPTIONS = {'linenos': 'table'}
