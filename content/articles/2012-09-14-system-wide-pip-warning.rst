Check Yo Self: System wide pip warning
######################################
:date: 2012-09-14T00:42:25.994Z
:slug: system-wide-pip-warning

Python's ``virtualenv`` is great. The ability to sandbox different apps from
one another and install otherwise conflicting dependencies makes deploying
multiple apps painless instead of the headache it would otherwise be. Combine
this with ``pip`` and a ``requirements.txt``, life could not be sweeter. That
is - if you do not forget to activate the virtual environment!

A few times, I forgot to activate the virtual environment for an app before
running ``pip install -r requirements.txt``. This of course invoked the
system-wide ``pip``, instead of the one for the app. As I am in the ``staff``
group on these systems, I have write access to the system-wide
``/usr/local/lib/python2.7/site-packages/``, ``pip`` would not even throw a
permission error to alert me. Basically, I would not notice that the packages
were installed to the wrong spot, and then I would wonder why my app complained
of missing packages!

Fixing this one is a simple job. One ``bash`` function later, and my shell asks
me if I really want to invoke the system-wide ``pip`` before it runs.

Put this in your ``~/.bashrc``:

.. code-block:: bash

    # Warn about using the global pip. This usually means we forgot to activate a
    # virtualenv
    system_pip=`which pip`
    function pip() {
        current_pip=`which pip`
        if [[ "$current_pip" == "$system_pip" ]] ; then
            echo "You are using the system-wide pip."
            read -r -p "Are you sure you want to do this? [y/N] " response
            case $response in
                [yY]) $current_pip $@ ;;
                *) ;;
            esac
        else
            $current_pip $@
        fi
    }

Simple, and has saved me from a simple mistake a couple of times already.
