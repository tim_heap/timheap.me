grep-then-$EDITOR shortcut
##########################
:date: 2012-11-19T05:00:20.142Z
:slug: grep-then-editor-shortcut
:summary: I got sick of typing ``vi $( ack-grep 'pattern' )``, so I made a shortcut for it...

I got sick of typing ``vi $( ack-grep 'pattern' )``, so I made a shortcut for
it:

.. code-block:: bash

    # in ~/.bashrc
    function ack-edit() {
        vi $( ack-grep -l "$@" )
    }

Now I can just type the following in your terminal to grep and edit the matching files:

.. code-block:: shell

    $ ack-edit 'pattern'

Nothing complex, but I've already used it multiple times today. You can of
course replace ``vi`` with your preferred editor, or even ``$EDITOR``.

This also uses ``ack`` instead of ``grep``. If you have not heard of ``ack``, I
highly recommend you go `read all about it <http://betterthangrep.com/>`_. It
will make your life better.
