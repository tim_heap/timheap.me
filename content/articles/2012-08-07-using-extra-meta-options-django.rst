Using extra Meta options in Django
##################################
:date: 2012-08-07T11:44:32.483Z
:slug: using-extra-meta-options-django
:summary: Extending Django models with extra Meta options...

The other day I was creating a Django app. It had some models that extended
data pulled in from an API. There were multiple models that all used the same
API, but pulled data from different end points, so I made an abstract Model
base class with some common functionality. Each concrete subclass then needed
to specify which endpoint it was using. This sounded like an excellent place to
use the Meta class that each Django Model has:

.. code-block:: python

    # in models.py
    from django.db import models
    from app import api

    class ApiExtension(models.Model):
        code = models.CharField(max_length=10)

        def get_data_from_api(self):
            return api.get_data(self._meta.api_model, self.pk)

    class Foo(ApiExtension):
        description = models.TextField()

        class Meta:
            api_model = 'foo'

Unfortunately, this does not work! Django (`sensibly or otherwise
<https://code.djangoproject.com/ticket/5793>`_) does NOT allow developers to
add random keys to the Meta class of models. We could add ``api_model`` to our
Model, but the Meta class provides such a nice, clear way of separating
configuration from data, and I really wanted to emulate it.

`EasyTree <https://bitbucket.org/fivethreeo/django-easytree>`_ have implemented
`their own Meta class implementation
<https://bitbucket.org/fivethreeo/django-easytree/src/7ab11cd55b09/easytree/models.py>`_.
Emulating that, I created my own Meta class, ApiMeta. The code I wrote is
below. It is a complete implementation of a custom Meta class for Models, which
you can adapt as you need for your own projects:

.. code-block:: python

    # in models.py
    class ApiOptions(object):
        """
        Options for API extensions
        """
        # Used in get_api_choices(), and _get_api_data()
        api_model = None

        # Each field named here is turned in to a @property, which calls the API
        # when accessed
        api_fields = []

        def __init__(self, opts):
            """
            Set any options provided, replacing the default values
            """
            if opts:
                for key, value in opts.__dict__.iteritems():
                    setattr(self, key, value)

    class ApiModelBase(models.base.ModelBase):
        """
        Metaclass for API extensions. Deals with values on ApiOptions that need
        preprocessing, such as `api_fields`
        """
        def __new__(mcs, name, bases, attrs):
            new = super(ApiModelBase, mcs).__new__(mcs, name, bases, attrs)

            # Grab `Model.ApiMeta`, and rename it `_api_meta`
            api_opts = attrs.pop('ApiMeta', None)
            setattr(new, '_api_meta', ApiOptions(optomate_opts))

            # Create some properties on the model, based upon `api_fields`
            for field in new._api_meta.api_fields:
                setattr(new, field, property(lambda s: s._get_api_field(field)))

            return new

    class ApiExtension(models.Model):

        __metaclass__ = ApiModelBase

        code = models.CharField(max_length=10)

        class Meta:
            abstract = True

        def _get_api_field(self, name):
            """
            Get `name` field from the API. Use the `Model.ApiMeta.api_model` option
            to determine which API model to query
            """
            data = api.get_instance(self._api_meta.model, self.code)
            if data:
                return data.get(name, None)
            else:
                return None

        def save(self, *args, **kwargs):
            if not self.name:
                self.name = self.get_name()

            return super(ApiExtension, self).save(*args, **kwargs)

        def get_name(self):
            name = self._get_api_field('name')
            if name:
                return name
            else:
                return '%s - Not in Api' % self.optomate_code

        @classmethod
        def get_api_choices(cls):
            """
            Get a list of tuples, suitable for a MultipleChoice widget
            """
            data = api.get_list(cls._api_meta.model)
            choices = [(o['code'], '%(code)s - %(name)s' % o) for o in data]
            return choices

        def get_resource_uri(self):
            return '%s/%s/%s/' % (settings.API_PATH,
                self._api_meta.model, self.code)
