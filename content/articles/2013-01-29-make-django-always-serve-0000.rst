Make Django always serve on 0.0.0.0
###################################
:date: 2013-01-29T05:37:20.215Z
:slug: make-django-always-serve-0000

I am working on a project at the moment that has to integrate with the
`Braintree <https://www.braintreepayments.com/>`_ payment gateway. It is a very
nice payment gateway, with a native Python API provided by the company, which
is always a nice touch!

When an action occurs in Braintree, such as a subscription changing state or a
charge being made against a subscription, Braintree POSTs at our application to
inform us of the action - a webhook. My testing application has to listen to
and accept these webhooks as they happen, so my Django application has to be
available to the world. A fairly standard ``ProxyPass`` configuration was set
up in Apache to point to my development Django instance, which made it
accessible to the outside world. Now, all I had to do was run ``./manage.py
runserver 0.0.0.0:8000`` to make the application visible externally.

That said, I always forgot to add the ``0.0.0.0:8000`` to make it visible
externally! This made testing really annoying. This application should never
run and not be externally visible, as the webhooks from Braintree are a core
part of the functionality. There was no way I could see to change the default
address to bind to in configurations, so I hacked it in to the `./manage.py`
script:

.. code-block:: python

    #!/usr/bin/env python
    import os
    import sys

    if __name__ == "__main__":
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.settings")

        from django.core.management import execute_from_command_line

        args = sys.argv[:]  # Grab a copy of argv
        if len(args) == 2 and sys.argv[1] == 'runserver':
            # Default to serving externally if not told otherwise
            args = sys.argv + ['0.0.0.0:8000']

        # Continue as normal
        execute_from_command_line(args)
