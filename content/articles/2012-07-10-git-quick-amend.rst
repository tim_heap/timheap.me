git quick-amend
###############
:date: 2012-07-10T06:29:41.390Z
:slug: git-quick-amend
:summary: Chuck this in your ``~/.gitconfig`` if you want to amend a commit with out editing its commit message...

Chuck this in your ``~/.gitconfig`` if you want to amend a commit with
out editing its commit message:

.. code-block:: ini

    # ~/.gitconfig
    [alias]
            quick-amend = !VISUAL=/bin/true git commit --amend

`/bin/true <http://ss64.com/bash/true.html>`_ is a standard
program that does nothing but exit with code ``0``. It is essentially a noop,
which is exactly what we want in this situation.
