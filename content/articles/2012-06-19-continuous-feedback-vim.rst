Continuous feedback in Vim
##########################
:date: 2012-06-19T12:55:26.319Z
:slug: continuous-feedback-vim

I introduced a colleague to `JSHint <http://jshint.com/>`_ today. JSHint is a
tool that checks your JavaScript for not just syntax errors, but strange and
ambiguous code. It catches things like extra commas (fails in some Webkits and
IEs), missing semicolons, undeclared globals, and potentially coercive equality
checks. All of these things are technically legal JavaScript, but they are best
avoided.

Constantly copy-pasting from his editor (Sublime Text 2) to the JSHint website
was tedious and error prone. Sublime Text is very extensible, so I was not
surprised to find a JSHint plugin for Sublime Text that automatically evaluates
your code as you type.

When I got back to my computer, I was actually sad that I did not have this
integration in Vim. An hour later though, and I did!

Check out `linters.vim <https://bitbucket.org/tim_heap/linters.vim/>`_.

Now, when ever I save a JavaScript file, a linter is run. Any errors reported
are show in the Vim quickfix window, ready for me to fix.

This speeds up my JavaScript development by quite a large margin! No longer do
I have to load the script up in a browser to spot some small (or even large)
syntax error, and strange IE7 bugs related to extra commas will not last long
in the codebase. Additionally, it will pick up on <code
data-language="javascript">console.log`` and its ilk as undefined globals - I
won't miss them this way, but can still safely keep them while developing.

I promptly did the same for LESS, replacing the ad-hoc solution I had made ages
ago. ``.less`` files are automatically compiled when I save them, and any
errors are reported directly in the quickfix window for me to fix. Python soon
got the same treatment, although I am not totally happy with either of
`pylint <http://pypi.python.org/pypi/pylint>`_ or
`pyflakes <http://pypi.python.org/pypi/pyflakes/0.5.0>`_. Haskell uses
`HLint <http://community.haskell.org/~ndm/hlint/>`_.

That is all for now, although adding more linters in the future is possible.
Pull requests are welcome!
