Check Yo Self: Automatically executable scripts
###############################################
:date: 2012-09-19T02:08:17.816Z
:slug: automatically-executable-scripts

This one is not a huge problem, more of a minor annoyance. I write small
bash/python scripts to automate things on a regular basis. I also always forget
to ``chmod +x`` the script afterwards! This leads to me wondering why the
script I **just wrote** can not be found anywhere on my ``$PATH``,
or even run directly. Rather annoying.

This is a handy vim snippet I found on VimTips at some point in the past. I
must admit that I have forgotten who wrote it, unfortunately, but thank you to
the author if you ever read this.

Just put the following in your ``~/.vimrc``, and every time you ``:write`` a
file with a shebang (``#!``) at the top it will be made executable:

.. code-block:: vim

    " Automatically chmod +x for files starting with #!
    function! s:AutoChmodX()
        if getline(1) =~ "^#!"
            call system(printf("chmod +x %s", shellescape(expand('%'), 1)))
        endif
    endfunction

    au BufWritePost * call s:AutoChmodX()

Simple, but saves me a few seconds and a headache here and there.
