Turns out that Heroku is awesome
################################
:date: 2012-05-30T14:42:52.691Z
:slug: turns-out-heroku-awesome
:summary: In which I switch to Heroku for hosting

I had previously checked out Heroku, but their pricing page made all of their
services looks very, VERY over priced for a simple blog. Even worse, I totally
missed the fact that they had a free tier!

I no longer have any excuse, so I have finally created a blog for myself.

Over the coming weeks, I will post some of the things I've made recently. I
want to get in the habit of writing, so I already have some ideas lined up,
such as:

* A write up of `SecretSource <http://maelstrom.github.com/SecretSource/>`_.

* Gushing about how awesome I think Django is.

* What `django-pronouns <https://bitbucket.org/tim_heap/django-pronouns>`_ is,
  why it will make your life easier, and why it will make you a better person
  just by using it!

* Some thoughts on `n2e <https://bitbucket.org/tim_heap/n2e>`_, an idea I am
  working on to make name collection nicer for all users - no matter how their
  name is structured.

Stay tuned!
