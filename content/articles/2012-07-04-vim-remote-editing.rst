Vim remote editing
##################
:date: 2012-07-04T22:39:22.762Z
:slug: vim-remote-editing
:summary: Just because I always forget the syntax, here is how to edit a file remotely via Vim...

Just because I always forget the syntax, here is how to edit a file remotely
via Vim:

.. code-block:: shell

    $ vim scp://hostname/.vimrc # Note single slash after hostname
    $ vim scp://hostname//absolute/path/to/file.txt # Note double slash after hostname

You can edit files relative to your home directory, or an absolute path, by
using one or two slashes respectively after the hostname.

Combined with brace expansion in ``bash``, this is very useful when comparing a
local and remote file:

.. code-block:: shell

    $ vimdiff {~/,scp://hostname/}.vimrc # Edit your .vimrc, and .vimrc on spectre
