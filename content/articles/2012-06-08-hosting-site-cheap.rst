Hosting a site on the cheap
###########################
:date: 2012-06-08T07:15:56.404Z
:slug: hosting-site-cheap

Part of the reason I had never set up a blog till now is because of monetary
constraints. Money is fairly tight for me currently, and running a blog falls
quite far below eating on my list of priorities. The only reason this blog is
running at all is because I managed to get everything set up and working for
only the cost of the domain name.

Hosting the source code
-----------------------

This one is easy. I chose Bitbucket, as it is what we use at work, but I could
have gone with Github, Bitbucket, Google Code, Source Forge, or any number of
free source code hosting sites. You can find the repo for this blog over at
`bitbucket.org/tim_heap/blog <https://bitbucket.org/tim_heap/blog>`_.

Hosting the app
---------------

As mentioned previously, this site is hosted on Heroku. Heroku has a free tier,
if you want to host small sites. This is my seconds post. This is a small site.
If this site ever hits the front page of Reddit or Hacker News, I might be in
trouble, but I will cross that bridge when it happens.

Heroku does not support hosting static files from your site itself. You can set
up Django to serve static assets, but this is explicitly warned against in the
Django documentation. The Heroku guides advise you to use Amazon S3, but that
costs money. It may not cost much, but everything counts

Hosting static assets
---------------------

So, hosting on Heroku or Amazon is out. Luckily, Github can come to the rescue
this time!  Github has an awesome feature: hosting static pages for your
repositories.  Normally, this is used to host documentation and examples for
your repository, but can also be abused to host static content. I had
previously used this to host the `static assets
<http://maelstrom.github.com/tumblr-theme/style.css>`_ for a `Tumblr theme
<https://github.com/maelstrom/tumblr-theme>`_ I made.  I made a gh-pages branch
in my repo and pushed it to Github. I now have a static asset server, for free.

Hosting uploaded content
------------------------

This is one that I have not yet solved. I do not know where and how I will
serve user created content. Luckily, this is not a problem for me, as I am the
sole content creator for this blog. When - and if - I solve this problem, I
will be sure to update this post!
