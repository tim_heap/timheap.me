Check Yo Self
#############
:date: 2012-09-14T00:25:55.993Z
:slug: check-yo-self

As the wisdom from the ancient text tells us,

    You better check yo self before you wreck yo self

    -- Ice Cube, 1993

I found myself constantly making the same small but annoying mistakes at work.
Nothing large or terrible, but still annoying. Things like installing packages
using the system ``pip`` instead of the virtualenv ``pip``,
committing to ``master`` because I forgot to checkout ``dev``
again, or wondering why a script I wrote couldn't be found when I forgot to
make it executable. Little things, but the time wasted on these adds up.

I've written, hacked, or found some tools that catch me out when I make these
mistakes. The tool either warns me that I am about to do something stupid, asks
me if I forgot to do something, or just does the task I forgot for me,
depending upon the situation.

These tools don't do all that much, but the annoyance they prevent was worth
every minute of effort it took to write them.
