KRunner SSH addon
#################
:date: 2013-01-13T22:27:40.306Z
:slug: krunner-ssh-addon

KRunner SSH is an add-on for KRunner to launch SSH connections easily. It reads
your ~/.ssh/config file to find hosts that you have defined, and presents them
as quick actions in KRunner.

Find it on Github at `maelstrom/ssh-runner
<https://github.com/maelstrom/ssh-runner>`_ or Bitbucket at
`tim_heap/ssh-krunner <https://bitbucket.org/tim_heap/ssh-runner>`_. It
includes instructions for compiling and installing it on KDE. It should work on
any system where KDE runs, but you may have to install different packages.
