Recording and printing pronouns in applications
###############################################
:date: 2012-06-10T13:02:43.776Z
:slug: recording-and-printing-pronouns-applications

For the uninitiated, printing pronouns in an application appears as easy as
just switching off a male/female field in your person model. This, however,
fails to handle many real life situations. This post is intended to educate
developers on how this is a limited and naive view of the world, and what can
be done to deal with the issues with which they are now faced

First, some terminology
-----------------------

I've seen many sign up forms for applications ask for your sex, as opposed to
your gender. Knowing some ones sex is actually not useful to you, and you
should actually be asking for the persons gender. Many people don't know the
distinction, but in short: sex is biological and physical, while gender is
sociological. In a social setting, such as your application addressing or
talking about a user, you will want to use the sociological construct of
gender. The only time sex is relevant is in medical situations - and even there
it is only really of use between a person and their medical professionals. In
the wider population, an individuals sex and gender often line up, but often is
not always. Alienating a significant portion of the worlds population just on
your sign up page is not a good first step! Wikipedia has `more information on
this distinction <http://en.wikipedia.org/wiki/Sex_and_gender_distinction>`_,
if you are interested.

Using a gender field is better, but still not quite there. What you actually
want to be collecting is the users preferred set of pronouns. There are the
female and male pronoun sets you are likely familiar with, but also two others
in common english: They and It. "They" is used for groups of people, or for a
single person who prefers to not expose their gender. "It" is usually used for
genderless objects and things. There is also a bunch of constructed pronoun
sets, intended to be used in a gender neutral way. Again, Wikipedia has `more
information on this topic <http://en.wikipedia.org/wiki/Gender-neutral_pronoun#Modern_English>`_.

Why a simple female/male switch is doomed to fail
-------------------------------------------------

Quite simply, every potential user of your site may not neatly fit in to a
female/male box. There are an almost uncountable number of reasons for this,
but :

* The user may in fact be a robot. Seriously, the Wikipedia user database is
  filled with `'bots' <http://en.wikipedia.org/wiki/Wikipedia:Bots>`_, which
  automate many editing tasks. What pronouns should you use for an automated
  program?

* Some people do not feel comfortable revealing their gender.

* The user may represent a group of people, such as a corporation or business,
  which again does not have a gender.

* Some people do not fall within the gender binary, so a male/female switch
  will not apply.

This has not even started addressing the problem of referring to people using
their preferred pronouns! Just `check out this table
<http://en.wikipedia.org/wiki/English_personal_pronouns#Full_list_of_personal_pronouns>`_
to see what I am on about. It is complicated!

django-pronouns
---------------

To try and help myself and other developers respect the preferences of all of
their potential users, I made a Django model you can use in conjunction with a
`User profile model <https://docs.djangoproject.com/en/dev/topics/auth/#storing-additional-information-about-users>`_,
to easily record a users preferred pronouns. Even better, it provides some
handy shortcuts for printing out pronouns in which ever form you need in your
templates.

Using ``django-pronouns``
*************************

In your user profile model:

.. code-block:: python

    from django.db import models
    from django.contrib.auth.models import User

    from django_pronouns.models import Pronoun

    class UserProfile(models.Model):
        user = models.OneToOneField(User)

        name = models.CharField(max_length=255)
        dob = models.DateField()
        pronoun = models.ForeignKey(Pronoun)

``Pronoun`` is a normal Django model, like any other. Set up your profile forms
as normal for a ForeignKey field.

In your templates, you can use the pronoun field to easily print out copy
tailored to the individual user:

.. code-block:: html

    {{ user.pronoun.subject|title }} is awesome.
    It is {{ user.name|pluralize }} birthday today. Go wish {{ user.pronoun.object }} happy birthday!
    {{ user.name }} looked at {{ user.pronoun.reflexive }} in the mirror.
    {{ user.pronoun.possessive_determiner|title }} stuff is on the table.
    This guitar is {{ user.pronoun.possessive_pronoun }}.

``django-pronouns`` comes with a bunch of predefined pronoun sets:
Masculine/feminine, singular they, "it", and `Spivak pronouns
<http://c2.com/cgi/wiki?SpivakPronouns>`_. You can of course modify these
through the Django admin.
