Django URL redirect shortcut
############################
:date: 2012-09-17T03:48:37.541Z
:slug: django-url-redirect-shortcut
:summary: Redirect just from the urls file in Django

Redirect from ``urls.py`` to another named URL, without having to create a
whole new view. This has to be lazy, otherwise the URL you are attempting to
reverse to will not exist yet:

.. code-block:: python

    from django.core.urlresolvers import reverse
    from django.http import HttpResponseRedirect

    def lazy_redirect(name, args=None, kwargs=None,
        http_response=HttpResponseRedirect):
        """
        Make a redirect view directly from `urls.py`.  Takes the same arguments as
        `django.core.urlresolvers.reverse`, with an additional argument
        `http_response`. Use `http_response` to change the HttpResponse class used
        in the redirect. It defaults to `HttpResponseRedirect`.
        """

        def redirect(_):
            destination = reverse(name, args=args, kwargs=kwargs)
            return http_response(destination)

        return redirect


    urlpatterns = patterns('',
        url(r'^redirect-me/$',
            lazy_redirect('page.view_page', kwargs={'name': 'landing-page.html'})
    )

This was written prior to Django 1.4, which now has
`reverse_lazy() <https://docs.djangoproject.com/en/dev/topics/http/urls/#reverse-lazy>`_.
This is still useful in 1.4 though, to redirect to the page in one step.
