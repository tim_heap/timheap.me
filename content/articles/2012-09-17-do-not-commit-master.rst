Check Yo Self: Do not commit to master
######################################
:date: 2012-09-17T00:39:16.989Z
:slug: do-not-commit-master

We follow the git pattern of ``dev`` and ``master`` branches at work. You do
everything on ``dev`` or a topic branch as appropriate. When your changes are
ready to go live, you merge them in to ``master``, push, and deploy to
staging/production.

Unfortunately, as git does not have a 'merge to master' command, you have to
check out ``master`` and then merge ``dev`` into it. If you forget to check out
``dev`` again after this, you will then start committing to ``master`` - not
awesome. You can do some rebase trickery to restore a clean history, but it
would be better if you didn't have to do it at all.

This git hook warns you when you are about to commit to master. It still allows
you to commit to ``master`` if you really want to, but gives you a chance to
sanity check your decision:

.. code-block:: bash

    #!/bin/bash

    if [[ `git symbolic-ref HEAD` == "refs/heads/master" ]] ; then
        echo "You are about to commit to master."
        echo "You probably want to commit to another branch."

        # Hack to get stdin working again - hooks are not run in an interactive
        # environment. From http://stackoverflow.com/a/10015707/1283421
        exec &lt; /dev/tty

        read -r -p "Are you sure you want to do this? [y/N] " response
        case $response in
            [yY])
                exit 0
                ;;
            *)
                echo "Try the following commands:"
                echo "git stash"
                echo "git checkout dev"
                echo "git stash pop"
                echo "git commit"
                exit 1
                ;;
        esac
    fi
